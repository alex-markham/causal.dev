---
title: "MeDIL: A Python Package for Causal Modelling"
date: 2020-09-23T2:00:00.283621Z
publishDate: 2020-08-03T2:00:00.283621Z
authors: ["Alex Markham", "Aditya Chivukula", "Moritz Grosse-Wentrup"]
publication_types: [1]
abstract: "We present the `MeDIL` Python package for causal modelling. Its current features focus on (i) non-linear unconditional pairwise independence testing, (ii) constraint-based causal structure learning, and (iii) learning the corresponding functional causal models (FCMs), all for the class of measurement dependence inducing latent (MeDIL) causal models. MeDIL causal models and therefore the `MeDIL` software package are especially suited for analyzing data from fields such as psychometric, epidemiology, etc. that rely on questionnaire or survey data."
featured: false
draft: false
publication: "International Conference on Probabilistic Graphical Models (PGM)"
url_pdf: https://causal.dev/files/medil_demo.pdf
url_code: https://medil.causal.dev
---
