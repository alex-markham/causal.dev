(TeX-add-style-hook
 "xu_2021"
 (lambda ()
   (LaTeX-add-bibitems
    "xu2021dist")
   (LaTeX-add-environments
    '("algorithm" LaTeX-env-args ["argument"] 0)))
 :bibtex)

