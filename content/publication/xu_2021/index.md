---
title: "Distance Covariance: A Nonlinear Extension of Riemannian Geometry for EEG-based Brain-Computer Interfacing"
date: 2021-10-27T2:00:00.283621Z
publishDate: 2021-10-27T2:00:00.283621Z
authors: ["Jiachen Xu", "Alex Markham", "Anja Meunier", "Philipp Raggam", "Moritz Grosse-Wentrup"]
publication_types: [1]
abstract: "Riemannian frameworks are the basis for some of the best-performing decoding methods in EEG-based Brain-Computer Interfacing. In this work, we consider whether a nonlinear extension of the Riemannian framework, obtained by replacing the channel-wise covariance of the EEG signal with the nonlinear distance covariance, improves decoding performance. We study the theoretical properties of the distance covariance metric in this framework, in particular invariance to affine transformations, and compare the proposed method with established Riemannian methods on three different EEG data sets. We do not find evidence that the distance covariance extension improves decoding performance in comparison to the linear Riemannian framework."
featured: false
draft: false
publication: "2021 IEEE International Conference on Systems, Man, And Cybernetics (SMC)"

url_pdf: http://eprints.cs.univie.ac.at/7136/1/Paper%20Jiachen%20-%20Oct.%202021_SMC.pdf
---
