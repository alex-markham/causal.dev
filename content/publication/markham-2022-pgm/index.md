---
title: "A Transformational Characterization of Unconditionally Equivalent Bayesian Networks"
date: 2022-09-01T2:00:00.283621Z
publishDate: 2022-09-01T2:00:00.283621Z
authors: ["Alex Markham", "Danai Deligeorgaki", "Pratik Misra", "Liam Solus"]
publication_types: [1]
abstract: "We consider the problem of characterizing Bayesian networks
	  up to unconditional equivalence, i.e., when directed acyclic
	  graphs (DAGs) have the same set of unconditional
	  d-separation statements.  Each unconditional equivalence
	  class (UEC) is uniquely represented with an undirected graph
	  whose clique structure encodes the members of the class.
	  Via this structure, we provide a transformational
	  characterization of unconditional equivalence; i.e., we show
	  that two DAGs are in the same UEC if and only if one can be
	  transformed into the other via a finite sequence of
	  specified moves.  We also extend this characterization to
	  the essential graphs representing the Markov equivalence
	  classes (MECs) in the UEC.  UECs form a partition coarsening
	  of the space of MECs and are easily estimable from marginal
	  independence tests.  Thus, a characterization of
	  unconditional equivalence has applications in methods that
	  involve searching the space of MECs of Bayesian networks."
featured: false
draft: false
publication: "11th International Conference on Probabilistic Graphical Models (PGM)"

url_pdf: https://arxiv.org/pdf/2203.00521.pdf
url_slides: files/handout_pgm_slides.pdf
---
