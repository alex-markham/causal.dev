---
title: "Combinatorial and algebraic perspectives on the marginal
       independence structure of Bayesian networks"
date: 2022-10-04T2:00:00.283621Z
publishDate: 2022-10-041T2:00:00.283621Z
authors: ["Danai Deligeorgaki", "Alex Markham", "Pratik Misra", "Liam Solus"]
publication_types: [3]
abstract: "We consider the problem of estimating the marginal
	  independence structure of a Bayesian network from
	  observational data in the form of an undirected graph called
	  the unconditional dependence graph.  We show that
	  unconditional dependence graphs correspond to the graphs
	  having equal independence and intersection numbers.  Using
	  this observation, a Gröbner basis for a toric ideal
	  associated to unconditional dependence graphs is given and
	  then extended by additional binomial relations to connect
	  the space of unconditional dependence graphs.  An MCMC
	  method, called GrUES (Gröbner-based Unconditional
	  Equivalence Search), is implemented based on the resulting
	  moves and applied to synthetic Gaussian data.  GrUES
	  recovers the true marginal independence structure via a
	  BIC-optimal or MAP estimate at a higher rate than simple
	  independence tests while also yielding an estimate of the
	  posterior, for which the 20% HPD credible sets include
	  the true structure at a high rate for graphs with density at
	  least 0.5."
featured: false
draft: false
publication: "arXiv:2210.00822 [stat.ME]"

url_pdf: https://arxiv.org/pdf/2210.00822.pdf
url_code: https://gues.causal.dev/repro_astat.html
---
