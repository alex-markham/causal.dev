---
title: "Measurement Dependence Inducing Latent Causal Models"
date: 2020-08-06T2:00:00.283621Z
publishDate: 2020-08-04T2:00:00.283621Z
authors: ["Alex Markham", "Moritz Grosse-Wentrup"]
publication_types: [1]
abstract: "We consider the task of causal structure learning over measurement dependence inducing latent (MeDIL) causal models. We show that this task can be framed in terms of the graph theoretic problem of finding edge clique covers, resulting in an algorithm for returning minimal MeDIL causal models (minMCMs). This algorithm is non-parametric, requiring no assumptions about linearity or Gaussianity. Furthermore, despite rather weak assumptions about the class of MeDIL causal models, we show that _minimality_ in minMCMs implies some rather specific and interesting properties. By establishing MeDIL causal models as a semantics for edge clique covers, we also provide a starting point for future work further connecting causal structure learning to developments in graph theory and network science."
featured: false
draft: false
publication: "Conference on Uncertainty in Artificial Intelligence (UAI)"

url_pdf: http://www.auai.org/uai2020/proceedings/244_main_paper.pdf
url_code: https://medil.causal.dev
---
