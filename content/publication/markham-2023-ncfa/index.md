---
title: "Neuro-Causal Factor Analysis"
date: 2023-05-31T2:00:00.283621Z
publishDate: 2023-05-31T2:00:00.283621Z
authors: ["Alex Markham", "Mingyu Liu", "Bryon Aragam", "Liam Solus"]
publication_types: [3]
abstract: "We consider the task of causal structure learning over measurement dependence inducing latent (MeDIL) causal models. We show that this task can be framed in terms of the graph theoretic problem of finding edge clique covers, resulting in an algorithm for returning minimal MeDIL causal models (minMCMs). This algorithm is non-parametric, requiring no assumptions about linearity or Gaussianity. Furthermore, despite rather weak assumptions about the class of MeDIL causal models, we show that _minimality_ in minMCMs implies some rather specific and interesting properties. By establishing MeDIL causal models as a semantics for edge clique covers, we also provide a starting point for future work further connecting causal structure learning to developments in graph theory and network science."
featured: false
draft: false
publication: "arXiv:2305.19802 [stat.ML]"

url_pdf: https://arxiv.org/abs/2305.19802
url_code: https://medil.causal.dev/ncfa
---
