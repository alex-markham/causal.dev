---
title: "A Distance Covariance-based Kernel for Nonlinear Causal Clustering in Heterogeneous Populations"
date: 2022-02-20T2:00:00.283621Z
publishDate: 2022-02-20T2:00:00.283621Z
authors: ["Alex Markham", "Richeek Das", "Moritz Grosse-Wentrup"]
publication_types: [1]
abstract: "  We consider the problem of causal structure learning in the setting of heterogeneous populations, i.e., populations in which a single causal structure does not adequately represent all population members, as is common in biological and social sciences.  To this end, we introduce a distance covariance-based kernel designed specifically to measure the similarity between the underlying nonlinear causal structures of different samples.  Indeed, we prove that the corresponding feature map is a statistically consistent estimator of nonlinear independence structure, rendering the kernel itself a statistical test for the hypothesis that sets of samples come from different generating causal structures.  Even stronger, we prove that the kernel space is isometric to the space of causal ancestral graphs, so that distance between samples in the kernel space is guaranteed to correspond to distance between their generating causal structures.  This kernel thus enables us to perform clustering to identify the homogeneous subpopulations, for which we can then learn causal structures using existing methods.  Though we focus on the theoretical aspects of the kernel, we also evaluate its performance on synthetic data and demonstrate its use on a real gene expression data set."
featured: false
draft: false
publication: "Conference on Causal Learning and Reasoning (CLeaR)"

url_pdf: https://arxiv.org/pdf/2106.03480.pdf
url_slides: files/clear22_slides.pdf
url_code: code/dep_con_kernel.py
---
