---
# Display name
name: Alex Markham
pronouns: "[they/them](https://www.mypronouns.org/they-them)"

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Postdoc Researcher

# Organizations/Affiliations
organizations:
- name: Math of Data and AI, Department of Mathematics, KTH Royal Institute of Technology
  url: "https://www.kth.se/math/mathematics-of-data"

# Short bio (displayed in user profile at end of posts)
bio: I analyze and develop causal discovery algorithms, especially for applications in the cognitive and brain sciences, using tools from graph theory and algebraic statistics.

interests:
- Causality and Machine Learning
- Graph Theory and Algebraic Statistics
- Methodology and Philosophy of Science

education:
  courses:
  - course: PhD in Computer Science
    institution: University of Vienna, Austria
    year: 2022
  - course: MS in Logic, Computation, and Methodology
    institution: Carnegie Mellon University, Pittsburgh, PA
    year: 2017
  - course: BA in Philosophy, BA in Mathematics
    institution: St. Mary's University, San Antonio, TX
    year: 2015

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: "mailto:alex.markham@causal.dev"
# - icon: twitter
#   icon_pack: fab
#   link: https://twitter.com/GeorgeCushen
- icon: semantic-scholar
  icon_pack: ai
  link: https://www.semanticscholar.org/author/Alex-Markham/40325765
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/alex-markham
- icon: github
  icon_pack: fab
  link: https://github.com/Alex-Markham
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
- icon: cv
  icon_pack: ai
  link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Researchers
- Visitors
---

I am a postdoc researcher at KTH Royal Institute of Technology in Stockholm, Sweden, working with Liam Solus in the Mathematics of Data and AI group.
My research interests include causality, machine learning, and the methodology and philosophy of science, especially when applied to the cognitive and brain sciences.
In particular, I develop causal discovery methods, using tools from a variety of mathematical fields, including combinatorics and algebraic statistics.
I did my PhD with Moritz Grosse-Wentrup in the Research Group Neuroinformatics at the University of Vienna, where we studied how neural activity gives rise to cognition and behavior, with a special focus on causal discovery methods, neuroimaging data, and brain-computer interfaces.


<!-- I analyze and develop causal inference algorithms, especially for applications of latent models in neuroscience and psychology. My biggest concern is whether the use of particular causal inference algorithms is justified---not only mathematically but also methodologically. In other words, in addition to providing valid causal inference methods, I also try to clearly describe, delimit, and emphasize their sound application, including (at least plausibly) satisfying their underlying assumptions and not exceeding or overstating their warranted conclusions. -->
