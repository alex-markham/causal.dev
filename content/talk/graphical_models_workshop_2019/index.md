---
title: A Causal Semantics for the Edge Clique Cover Problem
subtitle: Poster
authors: ["Alex Markham", "Moritz Grosse-Wentrup"]
summary: We present MeDIL causal models as a semantics for edge clique covers over undirected dependency graphs.

date: "2019-10-23T17:30:00Z"
date_end: "2019-10-23T19:30:00Z"
all_day: false

event: "Graphical Models: Conditional Independence and Algebraic Structures"
event_url: https://www.groups.ma.tum.de/en/general/events/graphical-models-conditional-independence-and-algebraic-structures/
location: 'Technical University of Munich'

url_poster: files/causal_semantics.pdf

tags:
- MeDIL
- graph theory
- causal inference
---

This is a poster I presented focusing on the graph theoretic aspects of my MeDIL Causal Model project at a Grapical Models workshop hosted by the Technical University of Munich.
