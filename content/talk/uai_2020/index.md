---
title: Measurement Independence Inducing Latent Causal Models
subtitle: Conference Presentation
authors: ["Alex Markham", "Moritz Grosse-Wentrup"]
summary: "We consider the task of causal structure learning over measurement dependence inducing latent (MeDIL) causal models. We show that this task can be framed in terms of the graph theoretical problem of finding edge clique covers, resulting in a simple algorithm for returning minimal MeDIL causal models (minMCMs). This algorithm is non-parametric, requiring no assumptions about linearity or Gaussianity. Furthermore, despite rather weak assumptions about the class of MeDIL causal models, we show that *minimality* in minMCMs implies three rather specific and interesting properties: first, minMCMs provide lower bounds on (i) the number of latent causal variables and (ii) the number of functional causal relations that are required to model a complex system at *any* level of granularity; second, a minMCM contains *no* causal links between the latent variables; and third, in contrast to factor analysis, a minMCM may require *more* Latent than measurement variables."

date: "2020-08-05T09:15:00Z"
date_end: "2020-08-05T09:30:00Z"
all_day: false

event: "Conference on Uncertainty in Artificial Intelligence (UAI)"
event_url: http://www.auai.org/uai2020/index.php
location: "Virtual"

publishDate: "2020-08-03T12:00:00Z"
draft: true

tags:
- MeDIL
- causal inference
---
