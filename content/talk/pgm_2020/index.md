---
title: "MeDIL: A Python Package for Causal Modelling"
subtitle: Conference Software Demonstration
authors: ["Alex Markham", "Aditya Chivukula", "Moritz Grosse-Wentrup"]
summary: "We present the `MeDIL` Python package for causal modelling. Its current features focus on (i) non-linear unconditional pairwise independence testing, (ii) constraint-based causal structure learning, and (iii) learning the corresponding functional causal models (FCMs), all for the class of measurement dependence inducing latent (MeDIL) causal models. MeDIL causal models and therefore the `MeDIL` software package are especially suited for analyzing data from fields such as psychometric, epidemiology, etc. that rely on questionnaire or survey data."

date: "2020-08-05T09:15:00Z"
date_end: "2020-08-05T09:30:00Z"
all_day: false

event:  "10th International Conference on Probabilistic Graphical Models (PGM)"
event_url: https://pgm2020.cs.aau.dk/
location: "Aalborg, Denmark"

publishDate: "2020-08-03T12:00:00Z"
draft: true

tags:
- MeDIL
- causal inference
---
