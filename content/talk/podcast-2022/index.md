---
title: "Causality, Graph Theory, and the Brain"
subtitle: "Podcast Interview in Abstract: The Future of Science"
authors: ["Alex Markham"]
summary: "Alex Markham is completing their Postdoc in the Math of Data and AI group at KTH Royal Institute of Technology in Sweden. Their research focuses on developing new algorithms for learning causal models from data. Causal discovery is especially appealing to more applied researchers, because it offers an intuitive framework for reasoning about why stuff happens and how we can influence it to happen differently. Alex finds causal discovery especially interesting because of the many different fields it draws from, including philosophy, cognitive science, and methodology, as well as computational and mathematical fields, like machine learning, statistics, graph theory, algebraic geometry, and combinatorics."

date: "2021-12-13T08:30:00Z"
all_day: true

event: "Podcast Interview in Abstract: The Future of Science"
event_url: "https://anchor.fm/abstractcast/episodes/Ep--73---Causality--Graph-Theory--The-Brain-ft--Alex-Markham-e1b7d3v"
location: "Online"

url_video: "https://anchor.fm/abstractcast/episodes/Ep--73---Causality--Graph-Theory--The-Brain-ft--Alex-Markham-e1b7d3v"
publishDate: "2021-12-13T08:30:00Z"
---
