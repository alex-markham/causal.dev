---
title: Undirected Graphs for Causal Structure Learning
subtitle: Invited seminar talk
authors: ["Alex Markham"]
summary: "Though undirected graphs offer less detailed representations of causal structure than DAGs or ancestral graphs, they are easier to work with.  In this talk, we discuss three ways of using undirected graphs for causal structure learning: (1) representing the structure of measurement dependence inducing latent (MeDIL) causal models, giving a causal semantics to edge clique covers of undirected graphs; (2) facilitating a causal graph kernel embedding using the distance covariance, connecting causal structure learning to kernel methods in machine learning; and (3) partitioning the space of DAGs into unconditional equivalence classes, drastically reducing the search space of greedy learning algorithms."

date: "2022-02-17T18:00:00Z"
all_day: false

event: "Causality Seminar"
event_url: https://ccc.inaoep.mx/~esucar/Proyectos/Causal%20Discovery.html
location: "(virtual) INAOE, Puebla, Mexico"

url_slides: files/mexico_seminar.pdf

publishDate: "2022-02-17T18:00:00Z"
---
