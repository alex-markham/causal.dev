---
title: Tutorial on Causality and Causal Graph Kernel Embedding
subtitle: Invited seminar talk
authors: ["Alex Markham"]
summary: "This tutorial will cover the basics of causal discovery, including: how the do-calculus
       extends the language of probability theory to give a formal framework for expressing causal
       relationships, how these causal relationships are represented by graphical models, and how
       these causal graphical models can be learned from data.
       We will also discuss some recent work at the intersection between causal discovery and
       kernel methods in machine learning."

date: "2022-04-01T18:00:00Z"
all_day: false

event: "Scool Machine Learning Seminar"
event_url: https://team.inria.fr/scool/news/#seminars
location: "(virtual) Inria, Lille, France"

url_slides: files/inria_tutorial.pdf

publishDate: "2022-04-01T18:00:00Z"
---
