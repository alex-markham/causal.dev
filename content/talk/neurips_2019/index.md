---
title: Measurement Independence Inducing Latent Causal Models
subtitle: Short Talk and Poster
authors: ["Alex Markham", "Moritz Grosse-Wentrup"]
summary: "We consider the task of causal structure learning over measurement dependence inducing latent (MeDIL) causal models. We show that this task can be framed in terms of the graph theoretical problem of finding edge clique covers, resulting in a simple algorithm for returning minimal MeDIL causal models (minMCMs). This algorithm is non-parametric, requiring no assumptions about linearity or Gaussianity. Furthermore, despite rather weak assumptions about the class of MeDIL causal models, we show that *minimality* in minMCMs implies three rather specific and interesting properties: first, minMCMs provide lower bounds on (i) the number of latent causal variables and (ii) the number of functional causal relations that are required to model a complex system at *any* level of granularity; second, a minMCM contains *no* causal links between the latent variables; and third, in contrast to factor analysis, a minMCM may require *more* Latent than measurement variables."

date: "2019-12-14T10:15:00Z"
date_end: "2019-12-14T11:00:00Z"
all_day: false

event: "Do the right thing: machine learning and causal inference for improved decision making"
event_url: http://tripods.cis.cornell.edu/neurips19_causalml/
location: "Workshop at NeurIPS 2019, Vancouver, Canada"

publishDate: "2019-11-01T12:00:00Z"

url_video: files/neurips_vid.webm
url_slides: files/neurips_slides.pdf
url_poster: files/neurips_poster.pdf

tags:
- MeDIL
- causal inference
---

I gave a short talk and presented a poster on my MeDIL Causal Model project at a Causal Machine Learning workshop at NeurIPS 2019, in Vancouver, Canada.
